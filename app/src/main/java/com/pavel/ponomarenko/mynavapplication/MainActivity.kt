package com.pavel.ponomarenko.mynavapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.pavel.ponomarenko.mynavapplication.dummy.DummyContent
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ListFragment.OnListFragmentInteractionListener {

    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {
        Log.i("Navigation", "item selected: $item")
       /* var bundle = Bundle()
        bundle.putString("param1", "Selected")
        bundle.putString("param2", "$item")*/
        val action = ListFragmentDirections.actionToParams()
        action.setParam1("Selected from safefar")
        action.setParam2(item.toString())
        findNavController(R.id.nav_host).navigate(action)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                findNavController(R.id.nav_host).navigate(R.id.main_dest)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                findNavController(R.id.nav_host).navigate(R.id.list_dest)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                var bundle = Bundle()
                bundle.putString("param1", "Lidia")
                bundle.putString("param2", "Oheiro")
                findNavController(R.id.nav_host).navigate(R.id.action_global_params_dest, bundle)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
